//BQtw, BQA, BQL, BQP
//有EC
const titleName = '[Zowie-ENUS]'
const hostnameExpect = "buy.benq.com"
const productInfoUrl = "https://zowie.benq.com/en-us/jcr:content.productinfo.json"
const productInfo = await request.get(productInfoUrl)
const productInfoJson = JSON.parse(productInfo)
//console.log(productInfoJson)
//如果以前有EC但現在沒了EC
const noEC = []
// for(let i =0; i<productInfoJson.length ; i++){
//     const noECCheck = productInfoJson[i].checkProduct
//     const noEcUrlBuy = ""//以前如果沒EC的話就是空的
//     if(noECCheck ==noEcUrlBuy || noECCheck.indexOf("us-buy")<0){
//         noEC.push(noECCheck)
//     }
// }
for(let i =0; i<productInfoJson.length ; i++){
    const ecType = productInfoJson[i].checkProduct
    const ecTypeEnable = "https://zowie.benq.com/api/magento/queryProduct/BQA/us-buy-zowie?skuids="
    const productPage = productInfoJson[i].url

    if(ecType !== ecTypeEnable){
        await this.page.goto(productPage+cicGA)
        const ecButtonSelector ="div.game-product-info-purchase-btn"
        // div.game-product-info-purchase-btn
        await this.page.waitForSelector(ecButtonSelector)
        const ecButtonInnerHtml = await this.page.$eval(ecButtonSelector, element => element.innerHTML);
        const addToCartCheck = ecButtonInnerHtml.indexOf("Add To Cart")
        const buyNowCheck = ecButtonInnerHtml.indexOf("Buy Now")
        if(addToCartCheck>0 || buyNowCheck>0){
            break
        }else{
            noEC.push(productPage)
        }
    }else{
        noEC.push(productPage)
        continue

    }
}
//console.log("noEC:",noEC)
if(noEC.length === productInfoJson.length){
    throw new Error(`${titleName}  had EC in the past, but now it did not have EC now.`)
}
// for(let i =0; i<productInfoJson.length ; i++){

//     const ecUrl = productInfoJson[i].checkProduct
//     const ecHostName = productInfoJson[i].buyNowlink
//     if(ecUrl !== "" && ecHostName.indexOf("-buy")>0 && ecHostName.indexOf(hostnameExpect)<0){
//         const hostNameActual = ecHostName.slice(8,20)//buy.benq.com
//         throw new Error(`${titleName}  EC Host Name is not ${hostnameExpect}, it is ${hostNameActual} now. Here is the URL ${ecHostName}`)
//     }
// }
for(let i =0; i<productInfoJson.length ; i++){
    const ecType = productInfoJson[i].checkProduct
    const ecTypeEnable = "https://zowie.benq.com/api/magento/queryProduct/BQA/us-buy-zowie?skuids="
    const productPage = productInfoJson[i].url
    if(ecType !== ecTypeEnable){
        await this.page.goto(productPage+cicGA)
        const buyNowButtonSelector ="#buy-btn"
        // div.game-product-info-purchase-btn
        await this.page.waitForSelector(buyNowButtonSelector)
        await this.page.click(buyNowButtonSelector)
        await this.page.waitForSelector(".page-wrapper")
        await this.page.waitForTimeout(10000)//等待10000毫秒
        const ecZowieurl = await this.page.url()
        console.log(ecZowieurl)
        //https://buy.benq.com/us-buy-zowie/checkout/cart/?cartId=1730986
        const zowieECHostnameExpect = "buy.benq.com"
        const zowieECHostnameActual = ecZowieurl.slice(8,20)
        const zowieECHostnameActualCheck = ecZowieurl.indexOf(zowieECHostnameExpect)
        const zowieECUrlnameExpect = "us-buy-zowie"
        const zowieECUrlnameActual = ecZowieurl.slice(21,33)
        console.log(zowieECHostnameActual)
        console.log(zowieECUrlnameActual)
        if(zowieECHostnameActualCheck>0 && ecZowieurl.indexOf("-buy")>0 && ecZowieurl.indexOf(zowieECHostnameExpect)<0){
            throw new Error(`${titleName}  EC Host Name is not ${zowieECHostnameExpect}, it is ${zowieECHostnameActual} now. Here is the URL ${productPage}`)
        }
        if(zowieECHostnameActualCheck>0 && ecZowieurl.indexOf("-buy")>0 && ecZowieurl.indexOf(zowieECHostnameExpect)>0){
            break
        }
    }else{
        continue
    }
}
for(let i =0; i<productInfoJson.length ; i++){
    const ecType = productInfoJson[i].checkProduct
    const ecTypeEnable = "https://zowie.benq.com/api/magento/queryProduct/BQA/us-buy-zowie?skuids="
    const productPage = productInfoJson[i].url
    if(ecType !== ecTypeEnable){
        await this.page.goto(productPage+cicGA)
        const buyNowButtonSelector ="#buy-btn"
        // div.game-product-info-purchase-btn
        await this.page.waitForSelector(buyNowButtonSelector)
        await this.page.click(buyNowButtonSelector)
        await this.page.waitForSelector(".page-wrapper")
        await this.page.waitForTimeout(10000)//等待10000毫秒
        const ecZowieurl = await this.page.url()
        console.log(ecZowieurl)
        //https://buy.benq.com/us-buy-zowie/checkout/cart/?cartId=1730986
        const zowieECUrlnameExpect = "us-buy-zowie"
        const zowieECUrlnameActual = ecZowieurl.slice(21,33)
        const zowieECUrlnameActualCheck = ecZowieurl.indexOf(zowieECUrlnameExpect)
        if(zowieECUrlnameActualCheck>0 && ecZowieurl.indexOf("-buy")>0 && ecZowieurl.indexOf(zowieECUrlnameExpect)<0){
            throw new Error(`${titleName}  EC Host Name is not ${zowieECUrlnameExpect}, it is ${zowieECUrlnameActual} now. Here is the URL ${productPage}`)
        }
        if(zowieECUrlnameActualCheck>0 && ecZowieurl.indexOf("-buy")>0 && ecZowieurl.indexOf(zowieECUrlnameExpect)>0){
            console.log(productPage)
            break
        }
    }else{
        continue
    }
}

//沒有EC
const titleName = '[B2C-ENAP]'
const productInfoUrl = "https://www.benq.com/en-ap/jcr:content.productinfo.json"
const productInfo = await request.get(productInfoUrl)
const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
for(let i =0; i<productInfoJson.length ; i++){
    const ecUrl = productInfoJson[i].buyNowlink
    //console.log(ecUrl)
    if(ecUrl !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
        //因為ENAP有一個產品有us-buy, 官網上沒開EC, 但是Product Json上有
        //https://www.benq.com/en-ap/projector/accessory/carrybag-ql01.html
        //buyNowlink: "https://buy.benq.com/us-buy/soft-carrying-case-for-ht3550-tk850-tk810-tk800m-ht2150st-mh750-mh760-ht2050-ht3050-ht2550-tk800.html"
        //Step1:先看該國家有沒有xx-buy ex:en-eu就是eu-buy
        const ownEc = "ap-buy"
        if(ecUrl.indexOf(ownEc)>0){
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
        //Step2:檢查現在有沒有EC可以用SimpleProduct檢查比較準
        if(productInfoJson[i].isSimpleProduct=="true"){
            await this.page.goto(productInfoJson[i].url+cicGA)
            const ecArea = "div.main_buy > a"
            await this.page.click(ecArea)
            const cartPageCheck = await this.page.url()
            if(cartPageCheck.indexOf("/checkout/cart/")>0){
                const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
                const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
                throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
            }
        }else{
            await this.page.goto(productInfoJson[i].url+cicGA)
            const notEC = "javascript:void(0)"
            const ecArea = "div.main_buy > a"
            const ecCheck = await this.page.$eval(ecArea, element=> element.getAttribute("href"))
            if(ecCheck.indexOf(notEC)<0){
                await this.page.click(ecArea)
                const cartPageCheck = await this.page.url()
                if(cartPageCheck.indexOf("/checkout/cart/")>0){
                    const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
                    const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
                    throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
                }
            }}
        }
}

//BQE
//有EC
const titleName = '[B2C-ENEU]'
const hostnameExpect = "shop.benq.eu"
const productInfoUrl = "https://www.benq.eu/en-eu/jcr:content.productinfo.json"
const productInfo = await request.get(productInfoUrl)
const productInfoJson = JSON.parse(productInfo)
//console.log(productInfoJson)
//如果以前有EC但現在沒了EC
const noEC = []
for(let i =0; i<productInfoJson.length ; i++){
    const noECCheck = productInfoJson[i].buyNowlink
    const noEcUrlBuy = ""//以前如果沒EC的話就是空的
    if(noECCheck ==noEcUrlBuy || noECCheck.indexOf("us-buy")<0){
        noEC.push(noECCheck)
    }
}
console.log("noEC:",noEC)
if(noEC === productInfoJson.length){
    throw new Error(`${titleName}  had EC in the past, but now it did not have EC now.`)
}
for(let i =0; i<productInfoJson.length ; i++){
    const ecHostName = productInfoJson[i].buyNowlink
    if(ecHostName !== "" && ecHostName.indexOf("-buy")>0 && ecHostName.indexOf(hostnameExpect)<0){
        const hostNameActual = ecHostName.slice(8,20)//buy.benq.com
        throw new Error(`${titleName}  EC Host Name is not ${hostnameExpect}, it is ${hostNameActual} now. Here is the URL ${ecHostName}`)
    }
}
//沒有EC
const titleName = '[B2C-DEAT]'
const productInfoUrl = "https://www.benq.eu/de-at/jcr:content.productinfo.json"
const productInfo = await request.get(productInfoUrl)
const productInfoJson = JSON.parse(productInfo)
//console.log(productInfoJson)
const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
for(let i =0; i<productInfoJson.length ; i++){
    const ecUrl = productInfoJson[i].buyNowlink
    //console.log(ecUrl)
    if(ecUrl !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
        //Step1:先看該國家有沒有xx-buy ex:en-eu就是eu-buy
        const ownEc = "at-buy"
        if(ecUrl.indexOf(ownEc)>0){
            const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
            const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
            throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
        }
        //Step2:檢查現在有沒有EC可以用SimpleProduct檢查比較準
        if(productInfoJson[i].isSimpleProduct=="true"){
            await this.page.goto(productInfoJson[i].url+cicGA)
            const ecArea = "div.main_buy > a"
            await this.page.click(ecArea)
            const cartPageCheck = await this.page.url()
            if(cartPageCheck.indexOf("/checkout/cart/")>0){
                const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
                const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
                throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
            }
        }else{
            await this.page.goto(productInfoJson[i].url+cicGA)
            const notEC = "javascript:void(0)"
            const ecArea = "div.main_buy > a"
            const ecCheck = await this.page.$eval(ecArea, element=> element.getAttribute("href"))
            if(ecCheck.indexOf(notEC)<0){
                await this.page.click(ecArea)
                const cartPageCheck = await this.page.url()
                //不少歐洲國家是eu-buy
                if(cartPageCheck.indexOf("eu-buy")>0 ){
                    const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
                    const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
                    throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
                }
            }}
        }
}
