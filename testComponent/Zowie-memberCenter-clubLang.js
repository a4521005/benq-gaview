//BQtw, BQA, BQL, BQP

const titleName = '[Zowie-ENUS]'
await this.page.waitForTimeout(5000)//等待5000毫秒
const signInSelector = 'body > header > nav.top-nav > div > ul > li:nth-child(5) > a.log_name'
await this.page.waitForSelector(signInSelector)
await this.page.click(signInSelector)
await this.page.waitForTimeout(10000)//等待10000毫秒
await this.page.waitForSelector('#AccountLogin')
const memberCenterClublangurl = await this.page.url()
//console.log("EN-US club-lang URL:",memberCenterClublangurl)
const clubExpect = "club.benq.com"
const clubActual = memberCenterClublangurl.slice(8,21)
const clubLangExpect = "lang=en-us"
// https://club.benq.com/ICDS/Home/BenQAuth?system_id=Gaming&function=Login&lang=en-us&return_url=https://zowie.benq.com%2Fen-us%2Findex.login.html
const clubLangActual = memberCenterClublangurl.slice(73,83)
console.log(clubLangActual)
//const clubLangRemoveLang = clubLangExpect.replace("lang="," ")
const clubActualCheck = memberCenterClublangurl.indexOf(clubExpect)
const clubLangActualCheck = memberCenterClublangurl.indexOf(clubLangExpect)
if(clubActualCheck <0 && clubLangActualCheck <0 ){
    throw new Error(`${titleName} member center URL does not include ${clubExpect} and club-lang is not ${clubLangExpect} , member center URL includes ${clubActual} and club-lang is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
}
if(clubActualCheck<0){
    throw new Error(`${titleName} member center URL does not include ${clubExpect}, it is ${clubActual} now. Here is the URL ${memberCenterClublangurl}`)
}
if(clubLangActualCheck <0 ){
    throw new Error(`${titleName} club-lang is not ${clubLangExpect}, it is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
}
// expect(b2bClublangurl).to.include('club.benq.com')
// expect(b2bClublangurl).to.include('lang=en-us')

//BQE
const titleName = '[B2C-ENEU]'
const signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area > a'
await this.page.waitForSelector(signInSelector)
await this.page.click(signInSelector)
await this.page.waitForTimeout(10000)//等待10000毫秒
await this.page.waitForSelector('#AccountLogin')
const memberCenterClublangurl = await this.page.url()
const clubExpect = "club.benq.eu"
const clubActual = memberCenterClublangurl.slice(8,20)
const clubLangExpect = "lang=en-eu"
const clubLangActual = memberCenterClublangurl.slice(71,81)
const clubActualCheck = memberCenterClublangurl.indexOf(clubExpect)
const clubLangActualCheck = memberCenterClublangurl.indexOf(clubLangExpect)
if(clubActualCheck <0 && clubLangActualCheck <0 ){
    throw new Error(`${titleName} member center URL does not include ${clubExpect} and club-lang is not ${clubLangExpect} , member center URL includes ${clubActual} and club-lang is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
}
if(clubActualCheck<0){
    throw new Error(`${titleName} member center URL does not include ${clubExpect}, it is ${clubActual} now. Here is the URL ${memberCenterClublangurl}`)
}
if(clubLangActualCheck <0 ){
    throw new Error(`${titleName} club-lang is not ${clubLangExpect}, it is ${clubLangActual} now. Here is the URL ${memberCenterClublangurl}`)
}
