//BQtw, BQA, BQL, BQP
//有EC
const titleName = '[B2C-ENUS]'
const productInfoUrl = "https://www.benq.com/en-us/jcr:content.productinfo.json"
const productInfo = await request.get(productInfoUrl)
const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
const ecUrlBuyExpect = "us-buy"
for(let i =0; i<productInfoJson.length ; i++){
    const ecUrl = productInfoJson[i].buyNowlink
    //console.log(ecUrl)
    if(ecUrl !== "" && ecUrl.indexOf("-buy")>0 && ecUrl.indexOf(ecUrlBuyExpect)<0){
        const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
        throw new Error(`${titleName}  EC Host Name is not ${ecUrlBuyExpect}, it is ${ecUrlBuyActual} now. Here is the URL ${ecUrl}`)
    }
}
//沒有EC
const titleName = '[B2C-ENAP]'
const productInfoUrl = "https://www.benq.com/en-ap/jcr:content.productinfo.json"
const productInfo = await request.get(productInfoUrl)
const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
for(let i =0; i<productInfoJson.length ; i++){
    const ecUrl = productInfoJson[i].buyNowlink
    //console.log(ecUrl)
    if(ecUrl !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
        const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
        const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
        throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
    }
}

//BQE
//有EC
const titleName = '[B2C-ENEU]'
const productInfoUrl = "https://www.benq.eu/en-eu/jcr:content.productinfo.json"
const productInfo = await request.get(productInfoUrl)
const productInfoJson = JSON.parse(productInfo)
    //console.log(productInfoJson)
const ecUrlBuyExpect = "eu-buy"//以前如果沒EC的話就是空的
for(let i =0; i<productInfoJson.length ; i++){
    const ecUrl = productInfoJson[i].buyNowlink
    //console.log(ecUrl)
    if(ecUrl !== "" && ecUrl.indexOf("-buy")>0 && ecUrl.indexOf(ecUrlBuyExpect)<0){
        const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
        throw new Error(`${titleName}  EC Host Name is not ${ecUrlBuyExpect}, it is ${ecUrlBuyActual} now. Here is the URL ${ecUrl}`)

    }
}

//沒有EC
const titleName = '[B2C-DEAT]'
const productInfoUrl = "https://www.benq.eu/de-at/jcr:content.productinfo.json"
const productInfo = await request.get(productInfoUrl)
const productInfoJson = JSON.parse(productInfo)
//console.log(productInfoJson)
const ecUrlBuyExpect = ""//以前如果沒EC的話就是空的
for(let i =0; i<productInfoJson.length ; i++){
    const ecUrl = productInfoJson[i].buyNowlink
    //console.log(ecUrl)
    if(ecUrl !== ecUrlBuyExpect && ecUrl.indexOf("-buy")>0){
        const ecUrlBuyActual = ecUrl.slice(21,27) //xx-buy
        const hostNameActual = ecUrl.slice(8,20)//buy.benq.com
        throw new Error(`${titleName}  did not have EC in the past, but now it has EC now. ${hostNameActual} is EC HostName, ${ecUrlBuyActual} is EC URL(xx-buy). Here is the URL ${ecUrl}`)
    }
}

