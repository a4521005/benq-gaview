//BQtw, BQA, BQL, BQP
const titleName = '[B2C-ENUS]'
const b2bNameExpect = 'en-us'//以前的結果
const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
// 取得name
const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.com/"," ")
const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
if(b2bURLCheck < 0 ){
    throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${enusB2bURL}`)
}
//BQE
const titleName = '[B2C-ENEU]'
const b2bNameExpect = 'en-eu'//以前的結果
const b2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
const b2bURL = await this.page.$eval(b2bURLSelector, element=> element.getAttribute("href"))
// 取得name
const b2bURLRemoveHostName = b2bURL.replace("https://www.benq.eu/"," ")
const b2bNameActual = b2bURLRemoveHostName.replace("/business/index.html"," ")//現在的結果
const b2bURLCheck = b2bNameActual.indexOf(b2bNameExpect)
if(b2bURLCheck < 0 ){
    throw new Error(`${titleName} B2B URL was ${b2bNameExpect} in the past, but it is ${b2bNameActual} now. Here is the URL ${enusB2bURL}`)
}