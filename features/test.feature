Feature:GA View Check
    tests:
    1. B2B URL
    2. Member Center(club-lang)
    3. EC Hostname and URL

    # B2C
    # Scenario: [B2C-ENUS] B2B URL
    #     Given Go to B2C-ENUS Page
    #     Then [B2C-ENUS] B2B URL must be en-us
    #     # Then [B2C-ENUS] It should have en-us_2B,2C_Master view
    #     # Then [B2C-ENUS] It should have en-us_B2B_Master view
    #     # Then [B2C-ENUS] It should have en-us_B2C_Master view
    # Scenario: [B2C-ENUS] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENUS Page
    #     When [B2C-ENUS] Go to Member Center
    #     Then [B2C-ENUS] club-lang must be en-us
    #     Then [B2C-ENUS] EC hostname must be buy.benq.com & EC URL must be us-buy

    # Scenario: [B2C-ENEU] B2B URL
    #     Given Go to B2C-ENEU Page
    #     Then [B2C-ENEU] B2B URL must be en-eu
    #     # Then [B2C-ENEU] It should have en-eu_2B,2C_Master view
    #     # Then [B2C-ENEU] It should have en-eu_B2B_Master view
    #     # Then [B2C-ENEU] It should have en-eu_B2C_Master view
    # Scenario: [B2C-ENEU] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENEU Page
    #     When [B2C-ENEU] Go to Member Center
    #     Then [B2C-ENEU] club-lang must be en-eu
    #     Then [B2C-ENEU] EC hostname must be shop.benq.eu && EC URL must be eu-buy

    # Scenario: [B2C-ENUK] B2B URL
    #     Given Go to B2C-ENUK Page
    #     Then [B2C-ENUK] B2B URL must be en-uk
    #     # Then [B2C-ENUK] It should have en-uk_2B,2C_Master view
    #     # Then [B2C-ENUK] It should have en-uk_B2B_Master view
    #     # Then [B2C-ENUK] It should have en-uk_B2C_Master view
    # Scenario: [B2C-ENUK] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENUK Page
    #     When [B2C-ENUK] Go to Member Center
    #     Then [B2C-ENUK] club-lang must be en-uk
    #     Then [B2C-ENUK] EC hostname must be shop.benq.eu & EC URL must be uk-buy

    # Scenario: [B2C-ENIE] B2B URL
    #     Given Go to B2C-ENIE Page
    #     Then [B2C-ENIE] B2B URL must be en-uk
    #     # Then [B2C-IE] It should have en-ie_B2C_Master view
    # Scenario: [B2C-ENIE] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENIE Page
    #     When [B2C-ENIE] Go to Member Center
    #     Then [B2C-ENIE] club-lang must be en-ie
    #     Then [B2C-ENIE] EC hostname must be shop.benq.eu & EC URL must be ie-buy
        
    # Scenario: [B2C-ENLU] B2B URL
    #     Given Go to B2C-ENLU Page
    #     Then [B2C-ENLU] B2B URL must be en-eu
    #     # Then [B2C-ENLU] It should have en-lu_B2C_Master view
    # Scenario: [B2C-ENLU] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENLU Page
    #     When [B2C-ENLU] Go to Member Center
    #     Then [B2C-ENLU] club-lang must be en-lu
    #     Then [B2C-ENLU] EC hostname must be shop.benq.eu & EC URL must be eu-buy
        
    # Scenario: [B2C-SVSE] B2B URL
    #     Given Go to B2C-SVSE Page
    #     Then [B2C-SVSE] B2B URL must be en-eu
    #     # Then [B2C-SVSE] It should have sv-se_B2C_Master view
    # Scenario: [B2C-SVSE] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-SVSE Page
    #     When [B2C-SVSE] Go to Member Center
    #     Then [B2C-SVSE] club-lang must be sv-se
    #     Then [B2C-SVSE] EC hostname must be shop.benq.eu & EC URL must be sc-buy

    # Scenario: [B2C-ENCEE] B2B URL
    #     Given Go to B2C-ENCEE Page
    #     Then [B2C-ENCEE] B2B URL must be en-eu
    #     # Then [B2C-ENCEE] It should have en-cee_B2C_Master view
    # Scenario: [B2C-ENCEE] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENCEE Page
    #     Then [B2C-ENCEE] B2C-ENCEE Page does not have Member Center
    #     Then [B2C-ENCEE] B2C-ENCEE Page does not have EC Hostname and URL
   
    # Scenario: [B2C-FRFR] B2B URL
    #     Given Go to B2C-FRFR Page
    #     Then [B2C-FRFR] B2B URL must be fr-fr
    #     # Then [B2C-FRFR] It should have fr-fr_2B,2C_Master view
    #     # Then [B2C-FRFR] It should have fr-fr_B2B_Master view
    #     # Then [B2C-FRFR] It should have fr-fr_B2C_Master view
    # Scenario: [B2C-FRFR] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-FRFR Page
    #     When [B2C-FRFR] Go to Member Center
    #     Then [B2C-FRFR] club-lang must be fr-fr
    #     Then [B2C-FRFR] EC hostname must be shop.benq.eu & EC URL must be fr-buy

    # Scenario: [B2C-FRCH] B2B URL
    #     Given Go to B2C-FRCH Page
    #     Then [B2C-FRCH] B2B URL must be fr-fr
    #     # Then [B2C-FRCH] It should have fr-ch_B2C_Master view
    # Scenario: [B2C-FRCH] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-FRCH Page
    #     When [B2C-FRCH] Go to Member Center
    #     Then [B2C-FRCH] club-lang must be fr-ch
    #     Then [B2C-FRCH] B2C-FRCH Page does not have EC Hostname and URL

    # Scenario: [B2C-ESES] B2B URL
    #     Given Go to B2C-ESES Page
    #     Then [B2C-ESES] B2B URL must be es-es
    #     # Then [B2C-ESES] It should have es-es_2B,2C_Master view
    #     # Then [B2C-ESES] It should have es-es_B2B_Master view
    #     # Then [B2C-ESES] It should have es-es_B2C_Master view
    # Scenario: [B2C-ESES] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ESES Page
    #     When [B2C-ESES] Go to Member Center
    #     Then [B2C-ESES] club-lang must be es-es
    #     Then [B2C-ESES] EC hostname must be shop.benq.eu & EC URL must be es-buy

    # Scenario: [B2C-ITIT] B2B URL
    #     Given Go to B2C-ITIT Page
    #     Then [B2C-ITIT] B2B URL must be it-it
    #     # Then [B2C-ITIT] It should have it-it_2B,2C_Master view
    #     # Then [B2C-ITIT] It should have it-it_B2B_Master view
    #     # Then [B2C-ITIT] It should have it-it_B2C_Master view
    # Scenario: [B2C-ITIT] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ITIT Page
    #     When [B2C-ITIT] Go to Member Center
    #     Then [B2C-ITIT] club-lang must be it-it
    #     Then [B2C-ITIT] EC hostname must be shop.benq.eu & EC URL must be it-buy

    # Scenario: [B2C-PTPT] B2B URL
    #     Given Go to B2C-PTPT Page
    #     Then [B2C-PTPT] B2B URL must be en-eu
    #     # Then [B2C-PTPT] It should have pt-pt_B2C_Master view
    # Scenario: [B2C-PTPT] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-PTPT Page
    #     When [B2C-PTPT] Go to Member Center
    #     Then [B2C-PTPT] club-lang must be pt-pt
    #     Then [B2C-PTPT] B2C-PTPT Page does not have EC Hostname and URL

    # Scenario: [B2C-DEDE] B2B URL
    #     Given Go to B2C-DEDE Page
    #     Then [B2C-DEDE] B2B URL must be de-de
    #     # Then [B2C-DEDE] It should have de-de_2B,2C_Master view
    #     # Then [B2C-DEDE] It should have de-de_B2B_Master view
    #     # Then [B2C-DEDE] It should have de-de_B2C_Master view
    # Scenario: [B2C-DEDE] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-DEDE Page
    #     When [B2C-DEDE] Go to Member Center
    #     Then [B2C-DEDE] club-lang must be de-de
    #     Then [B2C-DEDE] EC hostname must be shop.benq.eu & EC URL must be de-buy

    # Scenario: [B2C-DEAT] B2B URL
    #     Given Go to B2C-DEAT Page
    #     Then [B2C-DEAT] B2B URL must be de-de
    #      # Then [B2C-DEAT] It should have de-at_B2C_Master view
    # Scenario: [B2C-DEAT] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-DEAT Page
    #     When [B2C-DEAT] Go to Member Center
    #     Then [B2C-DEAT] club-lang must be de-de
    #     Then [B2C-DEAT] B2C-DEAT Page does not have EC Hostname and URL

    # Scenario: [B2C-DECH] B2B URL
    #     Given Go to B2C-DECH Page
    #     Then [B2C-DECH] B2B URL must be de-de
    #     # Then [B2C-DECH] It should have en-cee_B2C_Master view
    # Scenario: [B2C-DECH] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-DECH Page
    #     Then [B2C-DECH] B2C-DECH Page does not have Member Center
    #     Then [B2C-DECH] B2C-DECH Page does not have EC Hostname and URL

    # Scenario: [B2C-NLNL] B2B URL
    #     Given Go to B2C-NLNL Page
    #     Then [B2C-NLNL] B2B URL must be nl-nl
    #     # Then [B2C-NLNL] It should have nl-nl_2B,2C_Master view
    #     # Then [B2C-NLNL] It should have nl-nl_B2B_Master view
    #     # Then [B2C-NLNL] It should have nl-nl_B2C_Master view
    # Scenario: [B2C-NLNL] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-NLNL Page
    #     When [B2C-NLNL] Go to Member Center
    #     Then [B2C-NLNL] club-lang must be nl-nl
    #     Then [B2C-NLNL] EC hostname must be shop.benq.eu & EC URL must be nl-buy

    # Scenario: [B2C-NLBE] B2B URL
    #     Given Go to B2C-NLBE Page
    #     Then [B2C-NLBE] B2B URL must be nl-nl
    #      # Then [B2C-NLBE] It should have nl-nl_B2C_Master view
    # Scenario: [B2C-NLBE] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-NLBE Page
    #     When [B2C-NLBE] Go to Member Center
    #     Then [B2C-NLBE] club-lang must be nl-nl
    #     Then [B2C-NLBE] B2C-NLBE Page does not have EC Hostname and URL

    # Scenario: [B2C-ENNO] B2B URL
    #     Given Go to B2C-ENNO Page
    #     Then [B2C-ENNO] B2B URL must be en-eu
    #     # Then [B2C-ENNO] It should have en-cee_B2C_Master view
    # Scenario: [B2C-ENNO] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENNO Page
    #     Then [B2C-ENNO] B2C-ENNO Page does not have Member Center
    #     Then [B2C-ENNO] B2C-ENNO Page does not have EC Hostname and URL

    # Scenario: [B2C-ENFI] B2B URL
    #     Given Go to B2C-ENFI Page
    #     Then [B2C-ENFI] B2B URL must be en-eu
    #     # Then [B2C-ENFI] It should have en-fi_B2C_Master view
    # Scenario: [B2C-ENFI] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENFI Page
    #     When [B2C-ENFI] Go to Member Center
    #     Then [B2C-ENFI] club-lang must be en-fi
    #     Then [B2C-ENFI] EC hostname must be shop.benq.eu && EC URL must be sc-buy

    # Scenario: [B2C-ENDK] B2B URL
    #     Given Go to B2C-ENDK Page
    #     Then [B2C-ENDK] B2B URL must be en-eu
    #     # Then [B2C-ENDK] It should have en-dk_B2C_Master view
    # Scenario: [B2C-ENDK] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENDK Page
    #     When [B2C-ENDK] Go to Member Center
    #     Then [B2C-ENDK] club-lang must be en-dk
    #     Then [B2C-ENDK] EC hostname must be shop.benq.eu && EC URL must be sc-buy

    # Scenario: [B2C-ENIS] B2B URL
    #     Given Go to B2C-ENIS Page
    #     Then [B2C-ENIS] B2B URL must be en-eu
    #     # Then [B2C-ENIS] It should have en-cee_B2C_Master view
    # Scenario: [B2C-ENIS] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENIS Page
    #     Then [B2C-ENIS] B2C-ENIS Page does not have Member Center
    #     Then [B2C-ENIS] B2C-ENIS Page does not have EC Hostname and URL

    # Scenario: [B2C-RURU] B2B URL
    #     Given Go to B2C-RURU Page
    #     Then [B2C-RURU] B2B URL must be ru-ru
    #     # Then [B2C-RURU] It should have ru-ru_2B,2C_Master view
    #     # Then [B2C-RURU] It should have ru-ru_B2B_Master view
    #     # Then [B2C-RURU] It should have ru-ru_B2C_Master view
    # Scenario: [B2C-RURU] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-RURU Page
    #     Then [B2C-RURU] B2C-RURU Page does not have Member Center
    #     Then [B2C-RURU] B2C-RURU Page does not have EC Hostname and URL

    # Scenario: [B2C-PLPL] B2B URL
    #     Given Go to B2C-PLPL Page
    #     Then [B2C-PLPL] B2B URL must be pl-pl
    #     # Then [B2C-PLPL] It should have pl-pl_2B,2C_Master view
    #     # Then [B2C-PLPL] It should have pl-pl_B2B_Master view
    #     # Then [B2C-PLPL] It should have pl-pl_B2C_Master view
    # Scenario: [B2C-PLPL] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-PLPL Page
    #     When [B2C-PLPL] Go to Member Center
    #     Then [B2C-PLPL] club-lang must be en-eu
    #     Then [B2C-PLPL] EC hostname must be shop.benq.eu && EC URL must be eu-buy

    # Scenario: [B2C-BGBG] B2B URL
    #     Given Go to B2C-BGBG Page
    #     Then [B2C-BGBG] B2B URL must be en-eu
    #     # Then [B2C-BGBG] It should have bg-bg_B2C_Master view
    # Scenario: [B2C-BGBG] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-BGBG Page
    #     Then [B2C-BGBG] B2C-BGBG Page does not have Member Center
    #     Then [B2C-BGBG] B2C-BGBG Page does not have EC Hostname and URL

    # Scenario: [B2C-CSCZ] B2B URL
    #     Given Go to B2C-CSCZ Page
    #     Then [B2C-CSCZ] B2B URL must be cs-cz
    #     # Then [B2C-CSCZ] It should have cs-cz_2B,2C_Master view
    #     # Then [B2C-CSCZ] It should have cs-cz_B2B_Master view
    #     # Then [B2C-CSCZ] It should have cs-cz_B2C_Master view
    # Scenario: [B2C-CSCZ] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-CSCZ Page
    #     When [B2C-CSCZ] Go to Member Center
    #     Then [B2C-CSCZ] club-lang must be en-eu
    #     Then [B2C-CSCZ] EC hostname must be shop.benq.eu && EC URL must be eu-buy

    # Scenario: [B2C-ELGR] B2B URL
    #     Given Go to B2C-ELGR Page
    #     Then [B2C-ELGR] B2B URL must be en-eu
    #     # Then [B2C-ELGR] It should have el-gr_B2C_Master view
    # Scenario: [B2C-ELGR] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ELGR Page
    #     Then [B2C-ELGR] B2C-ELGR Page does not have Member Center
    #     Then [B2C-ELGR] B2C-ELGR Page does not have EC Hostname and URL

    # Scenario: [B2C-HUHU] B2B URL
    #     Given Go to B2C-HUHU Page
    #     Then [B2C-HUHU] B2B URL must be en-eu
    #     # Then [B2C-HUHU] It should have hu-hu_B2C_Master view
    # Scenario: [B2C-HUHU] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-HUHU Page
    #     Then [B2C-HUHU] B2C-HUHU Page does not have Member Center
    #     Then [B2C-HUHU] B2C-HUHU Page does not have EC Hostname and URL

    # Scenario: [B2C-LTLT] B2B URL
    #     Given Go to B2C-LTLT Page
    #     Then [B2C-LTLT] B2B URL must be en-eu
    #     # Then [B2C-LTLT] It should have lt-lt_B2C_Master view
    # Scenario: [B2C-LTLT] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-LTLT Page
    #     Then [B2C-LTLT] B2C-LTLT Page does not have Member Center
    #     Then [B2C-LTLT] B2C-LTLT Page does not have EC Hostname and URL
    
    # Scenario: [B2C-RORO] B2B URL
    #     Given Go to B2C-RORO Page
    #     Then [B2C-RORO] B2B URL must be en-eu
    #     # Then [B2C-RORO] It should have ro-ro_B2C_Master view
    # Scenario: [B2C-RORO] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-RORO Page
    #     Then [B2C-RORO] B2C-RORO Page does not have Member Center
    #     Then [B2C-RORO] B2C-RORO Page does not have EC Hostname and URL
    
    # Scenario: [B2C-SKSK] B2B URL
    #     Given Go to B2C-SKSK Page
    #     Then [B2C-SKSK] B2B URL must be en-eu
    #     # Then [B2C-SKSK] It should have sk-sk_B2C_Master view
    # Scenario: [B2C-SKSK] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-SKSK Page
    #     Then [B2C-SKSK] B2C-SKSK Page does not have Member Center
    #     Then [B2C-SKSK] B2C-SKSK Page does not have EC Hostname and URL

    # Scenario: [B2C-UKUA] B2B URL
    #     Given Go to B2C-UKUA Page
    #     Then [B2C-UKUA] B2B URL must be en-eu
    #     # Then [B2C-UKUA] It should have uk-ua_B2C_Master view
    # Scenario: [B2C-UKUA] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-UKUA Page
    #     Then [B2C-UKUA] B2C-UKUA Page does not have Member Center
    #     Then [B2C-UKUA] B2C-UKUA Page does not have EC Hostname and URL

    # Scenario: [B2C-ENLV] B2B URL
    #     Given Go to B2C-ENLV Page
    #     Then [B2C-ENLV] B2B URL must be en-eu
    #     # Then [B2C-ENLV] It should have en-lv_B2C_Master view
    # Scenario: [B2C-ENLV] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENLV Page
    #     Then [B2C-ENLV] B2C-ENLV Page does not have Member Center
    #     Then [B2C-ENLV] B2C-ENLV Page does not have EC Hostname and URL

    # Scenario: [B2C-ENRS] B2B URL
    #     Given Go to B2C-ENRS Page
    #     Then [B2C-ENRS] B2B URL must be en-eu
    #     # Then [B2C-ENRS] It should have en-rs_B2C_Master view
    # Scenario: [B2C-ENRS] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENRS Page
    #     Then [B2C-ENRS] B2C-ENRS Page does not have Member Center
    #     Then [B2C-ENRS] B2C-ENRS Page does not have EC Hostname and URL

    # Scenario: [B2C-ENSI] B2B URL
    #     Given Go to B2C-ENSI Page
    #     Then [B2C-ENSI] B2B URL must be en-eu
    #     # Then [B2C-ENSI] It should have en-si_B2C_Master view
    # Scenario: [B2C-ENSI] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENSI Page
    #     Then [B2C-ENSI] B2C-ENSI Page does not have Member Center
    #     Then [B2C-ENSI] B2C-ENSI Page does not have EC Hostname and URL

    # Scenario: [B2C-ENBA] B2B URL
    #     Given Go to B2C-ENBA Page
    #     Then [B2C-ENBA] B2B URL must be en-eu
    #     # Then [B2C-ENBA] It should have en-ba_B2C_Master view
    # Scenario: [B2C-ENBA] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENBA Page
    #     Then [B2C-ENBA] B2C-ENBA Page does not have Member Center
    #     Then [B2C-ENBA] B2C-ENBA Page does not have EC Hostname and URL

    # Scenario: [B2C-ENCY] B2B URL
    #     Given Go to B2C-ENCY Page
    #     Then [B2C-ENCY] B2B URL must be en-eu
    #     # Then [B2C-ENCY] It should have en-cy_B2C_Master view
    # Scenario: [B2C-ENCY] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENCY Page
    #     Then [B2C-ENCY] B2C-ENCY Page does not have Member Center
    #     Then [B2C-ENCY] B2C-ENCY Page does not have EC Hostname and URL

    # Scenario: [B2C-ENEE] B2B URL
    #     Given Go to B2C-ENEE Page
    #     Then [B2C-ENEE] B2B URL must be en-eu
    #     # Then [B2C-ENEE] It should have en-ee_B2C_Master view
    # Scenario: [B2C-ENEE] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENEE Page
    #     Then [B2C-ENEE] B2C-ENEE Page does not have Member Center
    #     Then [B2C-ENEE] B2C-ENEE Page does not have EC Hostname and URL

    # Scenario: [B2C-ENHR] B2B URL
    #     Given Go to B2C-ENHR Page
    #     Then [B2C-ENHR] B2B URL must be en-eu
    #     # Then [B2C-ENHR] It should have en-hr_B2C_Master view
    # Scenario: [B2C-ENHR] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENHR Page
    #     Then [B2C-ENHR] B2C-ENHR Page does not have Member Center
    #     Then [B2C-ENHR] B2C-ENHR Page does not have EC Hostname and URL

    # Scenario: [B2C-ENMK] B2B URL
    #     Given Go to B2C-ENMK Page
    #     Then [B2C-ENMK] B2B URL must be en-eu
    #     # Then [B2C-ENMK] It should have en-mk_B2C_Master view
    # Scenario: [B2C-ENMK] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENMK Page
    #     Then [B2C-ENMK] B2C-ENMK Page does not have Member Center
    #     Then [B2C-ENMK] B2C-ENMK Page does not have EC Hostname and URL

    # Scenario: [B2C-ENMT] B2B URL
    #     Given Go to B2C-ENMT Page
    #     Then [B2C-ENMT] B2B URL must be en-eu
    #     # Then [B2C-ENMT] It should have en-mt_B2C_Master view
    # Scenario: [B2C-ENMT] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENMT Page
    #     Then [B2C-ENMT] B2C-ENMT Page does not have Member Center
    #     Then [B2C-ENMT] B2C-ENMT Page does not have EC Hostname and URL

    # Scenario: [B2C-ENAP] B2B URL
    #     Given Go to B2C-ENAP Page
    #     Then [B2C-ENAP] B2B URL must be en-ap
    #     # Then [B2C-ENAP] It should have en-ap_2B,2C_Master view
    #     # Then [B2C-ENAP] It should have en-ap_B2B_Master view
    #     # Then [B2C-ENAP] It should have en-ap_B2C_Master view
    # Scenario: [B2C-ENAP] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENAP Page
    #     Then [B2C-ENAP] B2C-ENAP Page does not have Member Center
    #     Then [B2C-ENAP] B2C-ENAP Page does not have EC Hostname and URL
    
    # Scenario: [B2C-FRCA] B2B URL
    #     Given Go to B2C-FRCA Page
    #     Then [B2C-FRCA] B2B URL must be fr-ca
    #     # Then [B2C-FRCA] It should have fr-ca_2B,2C_Master view
    #     # Then [B2C-FRCA] It should have fr-ca_B2B_Master view
    #     # Then [B2C-FRCA] It should have fr-ca|fr-ca_B2C_Master view
    # Scenario: [B2C-FRCA] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-FRCA Page
    #     Then [B2C-FRCA] B2C-FRCA Page does not have Member Center
    #     Then [B2C-FRCA] B2C-FRCA Page does not have EC Hostname and URL

    # Scenario: [B2C-ENAU] B2B URL
    #     Given Go to B2C-ENAU Page
    #     Then [B2C-ENAU] B2B URL must be en-au
    #     # Then [B2C-ENAU] It should have en-au_2B,2C_Master view
    #     # Then [B2C-ENAU] It should have en-au_B2B_Master view
    #     # Then [B2C-ENAU] It should have en-au_B2C_Master view
    # Scenario: [B2C-ENAU] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENAU Page
    #     When [B2C-ENAU] Go to Member Center
    #     Then [B2C-ENAU] club-lang must be en-au
    #     Then [B2C-ENAU] EC hostname must be buy.benq.com & EC URL must be au-buy

    # Scenario: [B2C-ESAR] B2B URL
    #     Given Go to B2C-ESAR Page
    #     Then [B2C-ESAR] B2B URL must be es-la
    #     # Then [B2C-ESAR] It should have es-ar_B2C_Master view
    # Scenario: [B2C-ESAR] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ESAR Page
    #     Then [B2C-ESAR] B2C-ESAR Page does not have Member Center
    #     Then [B2C-ESAR] B2C-ESAR Page does not have EC Hostname and URL

    # Scenario: [B2C-ESLA] B2B URL
    #     Given Go to B2C-ESLA Page
    #     Then [B2C-ESLA] B2B URL must be es-la
    #     # Then [B2C-ESLA] It should have es-la_2B,2C_Master view
    #     # Then [B2C-ESLA] It should have es-la_B2B_Master view
    #     # Then [B2C-ESLA] It should have es-la_B2C_Master view
    # Scenario: [B2C-ESLA] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ESLA Page
    #     Then [B2C-ESLA] B2C-ESLA Page does not have Member Center
    #     Then [B2C-ESLA] B2C-ESLA Page does not have EC Hostname and URL

    # Scenario: [B2C-ENME] B2B URL
    #     Given Go to B2C-ENME Page
    #     Then [B2C-ENME] B2B URL must be en-ap
    #     # Then [B2C-ENME] It should have en-ap_B2C_Master view
    # Scenario: [B2C-ENME] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENME Page
    #     Then [B2C-ENME] B2C-ENME Page does not have Member Center
    #     Then [B2C-ENME] B2C-ENME Page does not have EC Hostname and URL

    # Scenario: [B2C-IDID] B2B URL
    #     Given Go to B2C-IDID Page
    #     Then [B2C-IDID] B2B URL must be en-ap
    #      # Then [B2C-IDID] It should have id-id_B2C_Master view
    # Scenario: [B2C-IDID] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-IDID Page
    #     When [B2C-IDID] Go to Member Center
    #     Then [B2C-IDID] club-lang must be id-id
    #     Then [B2C-IDID] B2C-IDID Page does not have EC Hostname and URL

    # Scenario: [B2C-TRTR] B2B URL
    #     Given Go to B2C-TRTR Page
    #     Then [B2C-TRTR] B2B URL must be tr-tr
    #     # Then [B2C-TRTR] It should have tr-tr_2B,2C_Master view
    #     # Then [B2C-TRTR] It should have tr-tr_B2B_Master view
    #     # Then [B2C-TRTR] It should have tr-tr_B2C_Master view
    # Scenario: [B2C-TRTR] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-TRTR Page
    #     Then [B2C-TRTR] B2C-TRTR Page does not have Member Center
    #     Then [B2C-TRTR] B2C-TRTR Page does not have EC Hostname and URL

    # Scenario: [B2C-KOKR] B2B URL
    #     Given Go to B2C-KOKR Page
    #     Then [B2C-KOKR] B2B URL must be ko-kr
    #     # Then [B2C-KOKR] It should have ko-kr_2B,2C_Master view
    #     # Then [B2C-KOKR] It should have ko-kr_B2B_Master view
    #     # Then [B2C-KOKR] It should have ko-kr_B2C_Master view
    # Scenario: [B2C-KOKR] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-KOKR Page
    #     Then [B2C-KOKR] B2C-KOKR Page does not have Member Center
    #     Then [B2C-KOKR] B2C-KOKR Page does not have EC Hostname and URL

    # Scenario: [B2C-THTH] B2B URL
    #     Given Go to B2C-THTH Page
    #     Then [B2C-THTH] B2B URL must be en-ap
    #      # Then [B2C-THTH] It should have th-th_B2C_Master view
    # Scenario: [B2C-THTH] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-THTH Page
    #     When [B2C-THTH] Go to Member Center
    #     Then [B2C-THTH] club-lang must be th-th
    #     Then [B2C-THTH] B2C-THTH Page does not have EC Hostname and URL

    # Scenario: [B2C-ENCA] B2B URL
    #     Given Go to B2C-ENCA Page
    #     Then [B2C-ENCA] B2B URL must be en-us
    #     # Then [B2C-ENCA] It should have en-ca_B2C_Master view
    # Scenario: [B2C-ENCA] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENCA Page
    #     Then [B2C-ENCA] B2C-ENCA Page does not have Member Center
    #     Then [B2C-ENCA] B2C-ENCA Page does not have EC Hostname and URL

    # Scenario: [B2C-ESCO] B2B URL
    #     Given Go to B2C-ESCO Page
    #     Then [B2C-ESCO] B2B URL must be es-la
    #     # Then [B2C-ESCO] It should have es-co_B2C_Master view
    # Scenario: [B2C-ESCO] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ESCO Page
    #     Then [B2C-ESCO] B2C-ESCO Page does not have Member Center
    #     Then [B2C-ESCO] B2C-ESCO Page does not have EC Hostname and URL

    # Scenario: [B2C-ENSG] B2B URL
    #     Given Go to B2C-ENSG Page
    #     Then [B2C-ENSG] B2B URL must be en-ap
    #     # Then [B2C-ENSG] It should have en-sg_B2C_Master view
    # Scenario: [B2C-ENSG] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENSG Page
    #     When [B2C-ENSG] Go to Member Center
    #     Then [B2C-ENSG] club-lang must be en-sg
    #     Then [B2C-ENSG] B2C-ENSG Page does not have EC Hostname and URL

    # Scenario: [B2C-ZHHK] B2B URL
    #     Given Go to B2C-ZHHK Page
    #     Then [B2C-ZHHK] B2B URL must be zh-hk
    #      # Then [B2C-ZHHK] It should have en-sg_2B,2C_Master view
    #      # Then [B2C-ZHHK] It should have en-sg_B2B_Master view
    #      # Then [B2C-ZHHK] It should have en-sg_B2C_Master view
    # Scenario: [B2C-ZHHK] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ZHHK Page
    #     When [B2C-ZHHK] Go to Member Center
    #     Then [B2C-ZHHK] club-lang must be zh-hk
    #     Then [B2C-ZHHK] B2C-ZHHK Page does not have EC Hostname and URL

    # Scenario: [B2C-ZHCN] B2B URL
    #     Given Go to B2C-ZHCN Page
    #     Then [B2C-ZHCN] B2B URL must be en-ap
    #      # Then [B2C-ZHCN] It should have zh-cn_B2C_Master view
    # Scenario: [B2C-ZHCN] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ZHCN Page
    #     When [B2C-ZHCN] Go to Member Center
    #     Then [B2C-ZHCN] club-lang must be zh-cn
    #     Then [B2C-ZHCN] B2C-ZHCN Page does not have EC Hostname and URL

    # Scenario: [B2C-ENIN] B2B URL
    #     Given Go to B2C-ENIN Page
    #     Then [B2C-ENIN] B2B URL must be en-ap
    #     # Then [B2C-ENIN] It should have en-in_B2C_Master view
    # Scenario: [B2C-ENIN] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENIN Page
    #     Then [B2C-ENIN] B2C-ENIN Page does not have Member Center
    #     Then [B2C-ENIN] B2C-ENIN Page does not have EC Hostname and URL

    # Scenario: [B2C-PTBR] B2B URL
    #     Given Go to B2C-PTBR Page
    #     Then [B2C-PTBR] B2B URL must be pt-br
    #     # Then [B2C-PTBR] It should have pt-br_2B,2C_Master view
    #     # Then [B2C-PTBR] It should have pt-br_B2B_Master view
    #     # Then [B2C-PTBR] It should have pt-br_B2C_Master view
    # Scenario: [B2C-PTBR] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-PTBR Page
    #     Then [B2C-PTBR] B2C-PTBR Page does not have Member Center
    #     Then [B2C-PTBR] B2C-PTBR Page does not have EC Hostname and URL

    # Scenario: [B2C-ESPE] B2B URL
    #     Given Go to B2C-ESPE Page
    #     Then [B2C-ESPE] B2B URL must be es-la
    #     # Then [B2C-ESPE] It should have es-pe_B2C_Master view
    # Scenario: [B2C-ESPE] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ESPE Page
    #     Then [B2C-ESPE] B2C-ESPE Page does not have Member Center
    #     Then [B2C-ESPE] B2C-ESPE Page does not have EC Hostname and URL

    # Scenario: [B2C-ENHK] B2B URL
    #     Given Go to B2C-ENHK Page
    #     Then [B2C-ENHK] B2B URL must be en-hk
    #     # Then [B2C-ENHK] It should have en-hk_2B,2C_Master view
    #     # Then [B2C-ENHK] It should have en-hk_B2B_Master view
    #     # Then [B2C-ENHK] It should have en-hk_B2C_Master view
    # Scenario: [B2C-ENHK] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENHK Page
    #     When [B2C-ENHK] Go to Member Center
    #     Then [B2C-ENHK] club-lang must be en-hk
    #     Then [B2C-ENHK] B2C-ENHK Page does not have EC Hostname and URL

    # Scenario: [B2C-ARME] B2B URL
    #     Given Go to B2C-ARME Page
    #     Then [B2C-ARME] B2B URL must be ar-me
    #     # Then [B2C-ARME] It should have ar-me_2B,2C_Master view
    #     # Then [B2C-ARME] It should have ar-me_B2B_Master view
    #     # Then [B2C-ARME] It should have ar-me_B2C_Master view
    # Scenario: [B2C-ARME] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ARME Page
    #     Then [B2C-ARME] B2C-ARME Page does not have Member Center
    #     Then [B2C-ARME] B2C-ARME Page does not have EC Hostname and URL

    # Scenario: [B2C-ESMX] B2B URL
    #     Given Go to B2C-ESMX Page
    #     Then [B2C-ESMX] B2B URL must be es-mx
    #     # Then [B2C-ESMX] It should have es-mx_2B,2C_Master view
    #     # Then [B2C-ESMX] It should have es-mx_B2B_Master view
    #     # Then [B2C-ESMX] It should have es-mx_B2C_Master view
    # Scenario: [B2C-ESMX] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ESMX Page
    #     When [B2C-ESMX] Go to Member Center
    #     Then [B2C-ESMX] club-lang must be es-mx
    #     Then [B2C-ESMX] EC hostname must be buy.benq.com & EC URL must be mx-buy

    # Scenario: [B2C-JAJP] B2B URL
    #     Given Go to B2C-JAJP Page
    #     Then [B2C-JAJP] B2B URL must be ja-jp
    #     # Then [B2C-JAJP] It should have ja-jp_2B,2C_Master view
    #     # Then [B2C-JAJP] It should have ja-jp_B2B_Master view
    #     # Then [B2C-JAJP] It should have ja-jp_B2C_Master view
    # Scenario: [B2C-JAJP] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-JAJP Page
    #     Then [B2C-JAJP] B2C-JAJP Page does not have Member Center
    #     Then [B2C-JAJP] B2C-JAJP Page does not have EC Hostname and URL

    # Scenario: [B2C-ENMY] B2B URL
    #     Given Go to B2C-ENMY Page
    #     Then [B2C-ENMY] B2B URL must be en-ap
    #     # Then [B2C-ENMY] It should have en-my_B2C_Master view
    # Scenario: [B2C-ENMY] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENMY Page
    #     When [B2C-ENMY] Go to Member Center
    #     Then [B2C-ENMY] club-lang must be en-my
    #     Then [B2C-ENMY] B2C-ENMY Page does not have EC Hostname and URL

    # Scenario: [B2C-VIVN] B2B URL
    #     Given Go to B2C-VIVN Page
    #     Then [B2C-VIVN] B2B URL must be en-ap
    #     # Then [B2C-VIVN] It should have vi-vn_B2C_Master view
    # Scenario: [B2C-VIVN] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-VIVN Page
    #     When [B2C-VIVN] Go to Member Center
    #     Then [B2C-VIVN] club-lang must be vi-vn
    #     Then [B2C-VIVN] B2C-VIVN Page does not have EC Hostname and URL

    # Scenario: [B2C-ZHTW] B2B URL
    #     Given Go to B2C-ZHTW Page
    #     Then [B2C-ZHTW] B2B URL must be zh-tw
    #     # Then [B2C-ZHTW] It should have zh-tw_2B,2C_Master view
    #     # Then [B2C-ZHTW] It should have zh-tw_B2B_Master view
    #     # Then [B2C-ZHTW] It should have zh-tw_B2C_Master view
    # Scenario: [B2C-ZHTW] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ZHTW Page
    #     When [B2C-ZHTW] Go to Member Center
    #     Then [B2C-ZHTW] club-lang must be zh-tw
    #     Then [B2C-ZHTW] EC hostname must be store.benq.com & EC URL must be tw-b2c

    # Scenario: [B2C-ENSE] B2B URL
    #     # 有設定轉址(en-se轉至sv-se)
    #     Given Go to B2C-ENSE Page
    #     Then [B2C-ENSE] B2B URL must be en-eu
    #     # Then [B2C-ENSE] It should have sv-se_B2C_Master view
    # Scenario: [B2C-ENSE] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to B2C-ENSE Page
    #     When [B2C-ENSE] Go to Member Center
    #     Then [B2C-ENSE] club-lang must be sv-se
    #     Then [B2C-ENSE] EC hostname must be shop.benq.eu & EC URL must be sc-buy

    # Zowie
    Scenario: [Zowie-ENUS] Member Center(club-lang) & EC Hostname and URL
        Given Go to Zowie-ENUS Page
        When [Zowie-ENUS] Go to Member Center
        Then [Zowie-ENUS] club-lang must be en-us
        Then [Zowie-ENUS] EC hostname must be buy.benq.com & EC URL must be us-buy-zowie

    # Scenario: [Zowie-ENUK] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-ENUK Page
    #     When [Zowie-ENUK] Go to Member Center
    #     Then [Zowie-ENUK] club-lang must be en-uk
    #     Then [Zowie-ENUK] EC hostname must be shop.benq.eu & EC URL must be uk-buy-zowie

    # Scenario: [Zowie-ENSE] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-ENSE Page
    #     When [Zowie-ENSE] Go to Member Center
    #     Then [Zowie-ENSE] club-lang must be en-se
    #     Then [Zowie-ENSE] EC hostname must be shop.benq.eu & EC URL must be se-buy-zowie

    # Scenario: [Zowie-ENIE] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-ENIE Page
    #     When [Zowie-ENIE] Go to Member Center
    #     Then [Zowie-ENIE] club-lang must be en-ie
    #     Then [Zowie-ENIE] EC hostname must be shop.benq.eu & EC URL must be ie-buy-zowie

    # Scenario: [Zowie-ENEU] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-ENEU Page
    #     When [Zowie-ENEU] Go to Member Center
    #     Then [Zowie-ENEU] club-lang must be en-eu
    #     Then [Zowie-ENEU] EC hostname must be shop.benq.eu & EC URL must be eu-buy-zowie

    # Scenario: [Zowie-ENNL] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-ENNL Page
    #     When [Zowie-ENNL] Go to Member Center
    #     Then [Zowie-ENNL] club-lang must be en-nl
    #     Then [Zowie-ENNL] EC hostname must be shop.benq.eu & EC URL must be nl-buy-zowie

    # Scenario: [Zowie-ENFR] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-ENFR Page
    #     When [Zowie-ENFR] Go to Member Center
    #     Then [Zowie-ENFR] club-lang must be en-fr
    #     Then [Zowie-ENFR] EC hostname must be shop.benq.eu & EC URL must be fr-buy-zowie

    # Scenario: [Zowie-ENIT] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-ENIT Page
    #     When [Zowie-ENIT] Go to Member Center
    #     Then [Zowie-ENIT] club-lang must be en-it
    #     Then [Zowie-ENIT] EC hostname must be shop.benq.eu & EC URL must be it-buy-zowie

    # Scenario: [Zowie-DEDE] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-DEDE Page
    #     When [Zowie-DEDE] Go to Member Center
    #     Then [Zowie-DEDE] club-lang must be de-de
    #     Then [Zowie-DEDE] EC hostname must be shop.benq.eu & EC URL must be de-buy-zowie

    # Scenario: [Zowie-ESES] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-ESES Page
    #     When [Zowie-ESES] Go to Member Center
    #     Then [Zowie-ESES] club-lang must be es-es
    #     Then [Zowie-ESES] EC hostname must be shop.benq.eu & EC URL must be es-buy-zowie
    
    # Scenario: [Zowie-RURU] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-RURU Page
    #     Then [Zowie-RURU] Zowie-RURU Page does not have Member Center
    #     Then [Zowie-RURU] Zowie-RURU Page does not have EC Hostname and URL

    # Scenario: [Zowie-PLPL] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-PLPL Page
    #     When [Zowie-PLPL] Go to Member Center
    #     Then [Zowie-PLPL] club-lang must be en-eu
    #     Then [Zowie-PLPL] EC hostname must be shop.benq.eu & EC URL must be eu-buy-zowie

    # Scenario: [Zowie-ENCA] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-ENCA Page
    #     Then [Zowie-ENCA] Zowie-ENCA Page does not have Member Center
    #     Then [Zowie-ENCA] Zowie-ENCA Page does not have EC Hostname and URL

    # Scenario: [Zowie-ENAP] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-ENAP Page
    #     Then [Zowie-ENAP] Zowie-ENAP Page does not have Member Center
    #     Then [Zowie-ENAP] Zowie-ENAP Page does not have EC Hostname and URL

    # Scenario: [Zowie-ZHTW] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-ZHTW Page
    #     When [Zowie-ZHTW] Go to Member Center
    #     Then [Zowie-ZHTW] club-lang must be zh-tw
    #     Then [Zowie-ZHTW] EC hostname must be store.benq.com & EC URL must be tw-zowie

    # Scenario: [Zowie-JAJP] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-JAJP Page
    #     Then [Zowie-JAJP] Zowie-JAJP Page does not have Member Center
    #     Then [Zowie-JAJP] Zowie-JAJP Page does not have EC Hostname and URL

    # Scenario: [Zowie-KOKR] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-KOKR Page
    #     Then [Zowie-KOKR] Zowie-KOKR Page does not have Member Center
    #     Then [Zowie-KOKR] Zowie-KOKR Page does not have EC Hostname and URL

    # Scenario: [Zowie-IDID] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-IDID Page
    #     Then [Zowie-IDID] Zowie-IDID Page does not have Member Center
    #     Then [Zowie-IDID] Zowie-IDID Page does not have EC Hostname and URL

    # Scenario: [Zowie-VIVN] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-VIVN Page
    #     Then [Zowie-VIVN] Zowie-VIVN Page does not have Member Center
    #     Then [Zowie-VIVN] Zowie-VIVN Page does not have EC Hostname and URL

    # Scenario: [Zowie-ENAU] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-ENAU Page
    #     Then [Zowie-ENAU] Zowie-ENAU Page does not have Member Center
    #     Then [Zowie-ENAU] Zowie-ENAU Page does not have EC Hostname and URL

    # Scenario: [Zowie-ENSG] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-ENSG Page
    #     Then [Zowie-ENSG] Zowie-ENSG Page does not have Member Center
    #     Then [Zowie-ENSG] Zowie-ENSG Page does not have EC Hostname and URL

    # Scenario: [Zowie-TRTR] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-TRTR Page
    #     Then [Zowie-TRTR] Zowie-TRTR Page does not have Member Center
    #     Then [Zowie-TRTR] Zowie-TRTR Page does not have EC Hostname and URL

    # Scenario: [Zowie-THTH] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-THTH Page
    #     Then [Zowie-THTH] Zowie-THTH Page does not have Member Center
    #     Then [Zowie-THTH] Zowie-THTH Page does not have EC Hostname and URL

    # Scenario: [Zowie-ARME] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-ARME Page
    #     Then [Zowie-ARME] Zowie-ARME Page does not have Member Center
    #     Then [Zowie-ARME] Zowie-ARME Page does not have EC Hostname and URL

    # Scenario: [Zowie-ZHCN] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-ZHCN Page
    #     Then [Zowie-ZHCN] Zowie-ZHCN Page does not have Member Center
    #     Then [Zowie-ZHCN] Zowie-ZHCN Page does not have EC Hostname and URL

    # Scenario: [Zowie-PTBR] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-PTBR Page
    #     Then [Zowie-PTBR] Zowie-PTBR Page does not have Member Center
    #     Then [Zowie-PTBR] Zowie-PTBR Page does not have EC Hostname and URL

    # Scenario: [Zowie-ESMX] Member Center(club-lang) & EC Hostname and URL
    #     Given Go to Zowie-ESMX Page
    #     Then [Zowie-ESMX] Zowie-ESMX Page does not have Member Center
    #     Then [Zowie-ESMX] Zowie-ESMX Page does not have EC Hostname and URL